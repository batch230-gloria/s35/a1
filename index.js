const express = require("express");

// Mongoose is a package that allows creation of Schemas to model our data structure
// Also has acces to a number of methods for manipulating database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [SECTION] MongoDB Collection
// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
/*
	Syntax:
		- mongoose.connect("<MongoDB connection string>", {urlNewUrlParser: true})
*/

mongoose.connect("mongodb+srv://admin:admin@batch230.zynmb2p.mongodb.net/S35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

// Connection to database
// Allows to handle errors when the initital connection is established
//  Works with the on and once Mongoose methods
let db = mongoose.connection

// If a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log(`We're connected to the cloud database`));

app.use(express.json());

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

// [POST]
app.post("/tasks", (request, response) =>{
	// Check if there are duplicate tasks

	Task.findOne({name: request.body.name}, (err,result)=>{
		// If there was found and the documnet's name matches the information via the client/Postman

		if(result!=null && result.name == request.body.name){
			return response.send("Duplicate task found");
		}
		else{
			let newTask = new Task({
				name: request.body.name
			})

			newTask.save((savErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New task created");
 				}
			})
		}
		
	})
})
	
// [GET]

app.get("/tasks", (req,res)=>{

	Task.find({}, (err,result)=>{

		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));







// [ACTIVITY]
	const userSchema = new mongoose.Schema({

	username: String,
	password: String
	
	})

	const User = mongoose.model("User", userSchema);

	app.post("/signup", (request, response) =>{

	User.findOne({username: request.body.username, password: request.body.password}, (err,result)=>{

		if(result!=null && result.username == request.body.username && result.password == request.body.password){
			return response.send("Duplicate user found");
		}
		else{
			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			})

			newUser.save((saveErr, savedUser)=>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New user created");
 				}
			})
		}
		
	})
})


